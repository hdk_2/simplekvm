/*
Copyright (c) 2014-2015 Hideki EIRAKU <hdk_2@users.sourceforge.net>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    (1) Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer. 

    (2) Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.  
    
    (3)The name of the author may not be used to
    endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

codestart:	.global	codestart
	.code16
	jmp	1f
	.org	codestart + 0x10
/*
RSD PTR @ 0xf0010
  0000: 52 53 44 20 50 54 52 20 61 41 00 00 00 00 00 00  ................
  0010: 30 00 0f 00 00 00 00 00 00 00 00 00 00 00 00 00  ................

RSDT @ 0xf0030
  0000: 52 53 44 54 28 00 00 00 01 88 41 20 20 20 20 20  ................
  0010: 41 20 20 20 20 20 20 20 00 00 00 00 41 20 20 20  ................
  0020: 00 00 00 00 60 00 0f 00 00 00 00 00 00 00 00 00  ................

FACP @ 0xf0060
  0000: 46 41 43 50 74 00 00 00 04 06 41 20 20 20 20 20  ................
  0010: 41 20 20 20 20 20 20 20 00 00 00 00 41 20 20 20  ................
  0020: 00 00 00 00 00 00 00 00 e0 00 0f 00 00 01 05 00  ................
  0030: b2 00 00 00 00 00 00 00 00 04 00 00 00 00 00 00  ................
  0040: 04 04 00 00 00 00 00 00 00 00 00 00 08 04 00 00  ................
  0050: 00 00 00 00 00 00 00 00 04 02 00 04 00 00 00 00  ................
  0060: ff 7f ff 7f 00 00 00 00 00 00 00 00 00 00 00 00  ................
  0070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................

DSDT @ 0xf00e0
  0000: 44 53 44 54 24 00 00 00 01 09 41 20 20 20 20 20  ................
  0010: 41 20 20 20 20 20 20 20 00 00 00 00 41 20 20 20  ................
  0020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
*/
	.long	0x20445352, 0x20525450, 0x00004161, 0x00000000 # RSD PTR
	.long	0x000f0030, 0x00000000, 0x00000000, 0x00000000
	.long	0x54445352, 0x00000028, 0x20418801, 0x20202020 # RSDT
	.long	0x20202041, 0x20202020, 0x00000000, 0x20202041
	.long	0x00000000, 0x000f0060, 0x00000000, 0x00000000
	.long	0x50434146, 0x00000074, 0x20410604, 0x20202020 # FACP
	.long	0x20202041, 0x20202020, 0x00000000, 0x20202041
	.long	0x00000000, 0x00000000, 0x000f00e0, 0x00050100
	.long	0x000000b2, 0x00000000, 0x00000400, 0x00000000
	.long	0x00000404, 0x00000000, 0x00000000, 0x00000408
	.long	0x00000000, 0x00000000, 0x04000204, 0x00000000
	.long	0x7fff7fff, 0x00000000, 0x00000000, 0x00000000
	.long	0x00000000, 0x00000000, 0x00000000, 0x00000000
	.long	0x54445344, 0x00000024, 0x20410901, 0x20202020 # DSDT
	.long	0x20202041, 0x20202020, 0x00000000, 0x20202041
	.long	0x00000000, 0x00000000, 0x00000000, 0x00000000
1:	lgdt	%cs:gdtr - codestart
	smsw	%ax
	inc	%ax
	lmsw	%ax
	ljmpl	$0x10, $1f - codestart + 0xf0000
	.code32
1:	xor	%eax, %eax
	mov	$0x18, %al
	mov	%eax, %ds
	mov	%eax, %es
	mov	%eax, %fs
	mov	%eax, %gs
	mov	%eax, %ss
	mov	$0x10000, %esi
	mov	$0xffffc, %esp
	ret
gdtr:	.short	0x1f
	.long	gdtr - codestart + 0xf0000 - 8
	.short	0
	.quad	0x00cf9b000000ffff
	.quad	0x00cf93000000ffff
codeend:	.global	codeend
