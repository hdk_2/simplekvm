/*
Copyright (c) 2014-2015 Hideki EIRAKU <hdk_2@users.sourceforge.net>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

    (1) Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer. 

    (2) Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.  
    
    (3)The name of the author may not be used to
    endorse or promote products derived from this software without
    specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

#include <linux/kvm.h>
#include <err.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <inttypes.h>
#include <stdlib.h>
#include <linux/elf.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>

static const int dump_initial_regs;
static const int simple_run_test;
static char *kernel_command_line = "";
static int console_initialized;

static void
dump_vram (char *mem)
{
  if (console_initialized)
    return;
  int x, y, l[25], m;
  for (y = 0; y < 25; y++)
    {
      char *p = &mem[0xb8000 + y * 160 + 158];
      for (l[y] = 79; l[y] >= 0; l[y]--, p -= 2)
	if (*p != ' ' && *p != '\0')
	  break;
    }
  for (m = 24; m >= 0; m--)
    if (l[m] >= 0)
      break;
  for (y = 0; y <= m; y++)
    {
      for (x = 0; x <= l[y]; x++)
	putchar (mem[0xb8000 + y * 160 + x * 2]);
      printf ("\r\n");
    }
}

static void
dump_vcpu_regs (int vcpufd)
{
  struct kvm_regs regs;
  if (ioctl (vcpufd, KVM_GET_REGS, &regs))
    err (1, "KVM_GET_REGS failed");
  printf ("rax: %08" PRIx64 " rbx: %08" PRIx64
	  " rcx: %08" PRIx64 " rdx: %08" PRIx64 "\r\n"
	  "rsi: %08" PRIx64 " rdi: %08" PRIx64
	  " rsp: %08" PRIx64 " rbp: %08" PRIx64 "\r\n"
	  "r8 : %08" PRIx64 " r9 : %08" PRIx64
	  " r10: %08" PRIx64 " r11: %08" PRIx64 "\r\n"
	  "r12: %08" PRIx64 " r13: %08" PRIx64
	  " r14: %08" PRIx64 " r15: %08" PRIx64 "\r\n"
	  "rip: %08" PRIx64 " rflags: %08" PRIx64 "\r\n",
	  (uint64_t)regs.rax, (uint64_t)regs.rbx,
	  (uint64_t)regs.rcx, (uint64_t)regs.rdx,
	  (uint64_t)regs.rsi, (uint64_t)regs.rdi,
	  (uint64_t)regs.rsp, (uint64_t)regs.rbp,
	  (uint64_t)regs.r8 , (uint64_t)regs.r9 ,
	  (uint64_t)regs.r10, (uint64_t)regs.r11,
	  (uint64_t)regs.r12, (uint64_t)regs.r13,
	  (uint64_t)regs.r14, (uint64_t)regs.r15,
	  (uint64_t)regs.rip, (uint64_t)regs.rflags);

  struct kvm_sregs sregs;
  if (ioctl (vcpufd, KVM_GET_SREGS, &sregs))
    err (1, "KVM_GET_SREGS failed");
  printf ("cs : [%04" PRIx16 "] %08" PRIx64 "-%08" PRIx32 " %02" PRIx8
	  " %" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8
	  ",%" PRIx8 ",%" PRIx8 " %" PRIx8 "\r\n"
	  "ds : [%04" PRIx16 "] %08" PRIx64 "-%08" PRIx32 " %02" PRIx8
	  " %" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8
	  ",%" PRIx8 ",%" PRIx8 " %" PRIx8 "\r\n"
	  "es : [%04" PRIx16 "] %08" PRIx64 "-%08" PRIx32 " %02" PRIx8
	  " %" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8
	  ",%" PRIx8 ",%" PRIx8 " %" PRIx8 "\r\n"
	  "fs : [%04" PRIx16 "] %08" PRIx64 "-%08" PRIx32 " %02" PRIx8
	  " %" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8
	  ",%" PRIx8 ",%" PRIx8 " %" PRIx8 "\r\n"
	  "gs : [%04" PRIx16 "] %08" PRIx64 "-%08" PRIx32 " %02" PRIx8
	  " %" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8
	  ",%" PRIx8 ",%" PRIx8 " %" PRIx8 "\r\n"
	  "ss : [%04" PRIx16 "] %08" PRIx64 "-%08" PRIx32 " %02" PRIx8
	  " %" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8
	  ",%" PRIx8 ",%" PRIx8 " %" PRIx8 "\r\n"
	  "tr : [%04" PRIx16 "] %08" PRIx64 "-%08" PRIx32 " %02" PRIx8
	  " %" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8
	  ",%" PRIx8 ",%" PRIx8 " %" PRIx8 "\r\n"
	  "ldt: [%04" PRIx16 "] %08" PRIx64 "-%08" PRIx32 " %02" PRIx8
	  " %" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8 ",%" PRIx8
	  ",%" PRIx8 ",%" PRIx8 " %" PRIx8 "\r\n"
	  "gdt: %08" PRIx64 "-%04" PRIx16 "\r\n"
	  "idt: %08" PRIx64 "-%04" PRIx16 "\r\n"
	  "cr0: %08" PRIx64 " cr2 : %08" PRIx64 " cr3 : %08" PRIx64
	  " cr4: %08" PRIx64 "\r\n"
	  "cr8: %08" PRIx64 " efer: %08" PRIx64 " apic: %08" PRIx64 "\r\n"
	  "interrupt_bitmap:\r\n"
	  " %016" PRIx64 " %016" PRIx64 " %016" PRIx64 " %016" PRIx64 "\r\n",
	  sregs.cs.selector, (uint64_t)sregs.cs.base, sregs.cs.limit,
	  sregs.cs.type,
	  sregs.cs.present, sregs.cs.dpl, sregs.cs.db, sregs.cs.s,
	  sregs.cs.l, sregs.cs.g, sregs.cs.avl, sregs.cs.unusable,
	  sregs.ds.selector, (uint64_t)sregs.ds.base, sregs.ds.limit,
	  sregs.ds.type,
	  sregs.ds.present, sregs.ds.dpl, sregs.ds.db, sregs.ds.s,
	  sregs.ds.l, sregs.ds.g, sregs.ds.avl, sregs.ds.unusable,
	  sregs.es.selector, (uint64_t)sregs.es.base, sregs.es.limit,
	  sregs.es.type,
	  sregs.es.present, sregs.es.dpl, sregs.es.db, sregs.es.s,
	  sregs.es.l, sregs.es.g, sregs.es.avl, sregs.es.unusable,
	  sregs.fs.selector, (uint64_t)sregs.fs.base, sregs.fs.limit,
	  sregs.fs.type,
	  sregs.fs.present, sregs.fs.dpl, sregs.fs.db, sregs.fs.s,
	  sregs.fs.l, sregs.fs.g, sregs.fs.avl, sregs.fs.unusable,
	  sregs.gs.selector, (uint64_t)sregs.gs.base, sregs.gs.limit,
	  sregs.gs.type,
	  sregs.gs.present, sregs.gs.dpl, sregs.gs.db, sregs.gs.s,
	  sregs.gs.l, sregs.gs.g, sregs.gs.avl, sregs.gs.unusable,
	  sregs.ss.selector, (uint64_t)sregs.ss.base, sregs.ss.limit,
	  sregs.ss.type,
	  sregs.ss.present, sregs.ss.dpl, sregs.ss.db, sregs.ss.s,
	  sregs.ss.l, sregs.ss.g, sregs.ss.avl, sregs.ss.unusable,
	  sregs.tr.selector, (uint64_t)sregs.tr.base, sregs.tr.limit,
	  sregs.tr.type,
	  sregs.tr.present, sregs.tr.dpl, sregs.tr.db, sregs.tr.s,
	  sregs.tr.l, sregs.tr.g, sregs.tr.avl, sregs.tr.unusable,
	  sregs.ldt.selector, (uint64_t)sregs.ldt.base, sregs.ldt.limit,
	  sregs.ldt.type,
	  sregs.ldt.present, sregs.ldt.dpl, sregs.ldt.db, sregs.ldt.s,
	  sregs.ldt.l, sregs.ldt.g, sregs.ldt.avl, sregs.ldt.unusable,
	  (uint64_t)sregs.gdt.base, sregs.gdt.limit,
	  (uint64_t)sregs.idt.base, sregs.idt.limit,
	  (uint64_t)sregs.cr0, (uint64_t)sregs.cr2, (uint64_t)sregs.cr3,
	  (uint64_t)sregs.cr4,
	  (uint64_t)sregs.cr8, (uint64_t)sregs.efer, (uint64_t)sregs.apic_base,
	  (uint64_t)sregs.interrupt_bitmap[0],
	  (uint64_t)sregs.interrupt_bitmap[1],
	  (uint64_t)sregs.interrupt_bitmap[2],
	  (uint64_t)sregs.interrupt_bitmap[3]);
}

static uint32_t
load_linux (char *filename, char *mem, uint64_t memsize, char *initrd)
{
  int fd = open (filename, O_RDONLY);
  if (fd < 0)
    err (1, "Open '%s'", filename);

  Elf32_Ehdr ehdr;
  uint32_t endaddr = 0;
  uint8_t buf[0x400];
  if (pread (fd, buf, sizeof buf, 0) == sizeof buf)
    {
      if (!memcmp (&buf[0x202], "HdrS", 4))
	{
	  off_t offset = buf[0x1f1] * 0x200 + 0x200;
	  off_t size = lseek (fd, 0, SEEK_END);
	  if (size > 0)
	    {
	      if (pread (fd, mem + 0x10000, offset, 0) != offset)
		err (1, "Read");
	      if (pread (fd, mem + 0x100000, size - offset, offset) !=
		  size - offset)
		err (1, "Read");
	      ehdr.e_entry = 0x100000;
	      endaddr = 0x100000 + size - offset;
	      goto bzimage;
	    }
	}
    }
  if (read (fd, &ehdr, sizeof ehdr) != sizeof ehdr)
    err (1, "Read ehdr");
  if (ehdr.e_ident[EI_MAG0] != ELFMAG0 || ehdr.e_ident[EI_MAG1] != ELFMAG1 ||
      ehdr.e_ident[EI_MAG2] != ELFMAG2 || ehdr.e_ident[EI_MAG3] != ELFMAG3)
    err (1, "Not ELF file");

  int i;
  for (i = 0; i < ehdr.e_phnum; i++)
    {
      Elf32_Phdr phdr;
      if (pread (fd, &phdr, sizeof phdr, ehdr.e_phoff + i * ehdr.e_phentsize)
	  != sizeof phdr)
	err (1, "Read phdr[%d]", i);
      if (phdr.p_type == PT_LOAD)
	{
	  if (phdr.p_memsz < phdr.p_filesz)
	    errx (1, "Invalid phdr");
	  if (phdr.p_paddr + phdr.p_memsz > memsize)
	    errx (1, "Out of memory");
	  memset (mem + phdr.p_paddr, 0, phdr.p_memsz);
	  if (pread (fd, mem + phdr.p_paddr, phdr.p_filesz, phdr.p_offset)
	      != phdr.p_filesz)
	    err (1, "Read PT_LOAD");
	  if (endaddr < phdr.p_paddr + phdr.p_memsz)
	    endaddr = phdr.p_paddr + phdr.p_memsz;
	}
    }
  memset (mem + 0x10000, 0, 0x1000);
 bzimage:
  close (fd);
  *(uint32_t *)&mem[0x101e0] = (memsize - 0x100000) / 1024;
  *(uint32_t *)&mem[0x10228] = 0x9f000;
  mem[0x10007] = 80;
  mem[0x1000e] = 24;
  *(uint16_t *)&mem[0x10010] = 16;

  if (initrd)
    {
      fd = open (initrd, O_RDONLY);
      if (fd < 0)
	err (1, "Open '%s'", initrd);
      off_t initrdsize = lseek (fd, 0, SEEK_END);
      if (initrdsize < 0)
	err (1, "Seek");
      if (memsize - initrdsize < endaddr)
	errx (1, "Out of memory for initrd");
      if (pread (fd, mem + memsize - initrdsize, initrdsize, 0) != initrdsize)
	err (1, "Read initrd");
      close (fd);
      mem[0x10210] = 1;
      *(uint32_t *)&mem[0x10218] = memsize - initrdsize;
      *(uint32_t *)&mem[0x1021c] = initrdsize;
    }
  snprintf (mem + 0x9f000, 0x1000, "console=ttyS0 %s", kernel_command_line);
  return ehdr.e_entry;
}

static int timer_interrupt_count;

static void
sigalrm (int signum)
{
  timer_interrupt_count++;
}

static void
unstty (void)
{
  system ("stty -raw echo");
}

static uint8_t
bcd (unsigned int bin)
{
  bin %= 100;
  return bin + 6 * (bin / 10);
}

int
main (int argc, char **argv)
{
  int opt;
  char *kernel = NULL;
  char *initrd = NULL;
  while ((opt = getopt (argc, argv, "k:i:c:")) != -1)
    {
      switch (opt)
	{
	case 'k':
	  kernel = optarg;
	  break;
	case 'i':
	  initrd = optarg;
	  break;
	case 'c':
	  kernel_command_line = optarg;
	  break;
	default:
	usage:
	  errx (1, "usage: %s -k vmlinux [-i initrd] [-c command-line]",
		argv[0]);
	}
    }
  if (!kernel)
    goto usage;

  /* Note: Parameter zero seems to be required for "Parameters: none"
     in The Definitive KVM (Kernel-based Virtual Machine) API
     Documentation. */

  /* Open */
  int fd = open ("/dev/kvm", O_RDONLY);
  if (fd < 0)
    err (1, "open");

  /* Check API version */
  if (ioctl (fd, KVM_GET_API_VERSION, 0) != 12)
    err (1, "API version mismatch");

  /* Check extensions */
  if (ioctl (fd, KVM_CHECK_EXTENSION, KVM_CAP_USER_MEMORY) <= 0)
    err (1, "KVM_CAP_USER_MEMORY not available");

  /* Create a VM */
  int vmfd = ioctl (fd, KVM_CREATE_VM, 0);
  if (vmfd < 0)
    err (1, "KVM_CREATE_VM failed");

  /* Allocate memory using mmap to get page-aligned address */
  struct kvm_userspace_memory_region memregion;
  memregion.slot = 0;
  memregion.flags = 0;
  memregion.guest_phys_addr = 0;
  memregion.memory_size = 256 * 1024 * 1024;
  char *mem = mmap (NULL, memregion.memory_size, PROT_READ | PROT_WRITE,
		    MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (mem == MAP_FAILED)
    err (1, "Allocation failed");
  memregion.userspace_addr = (uintptr_t)mem;
  if (ioctl (vmfd, KVM_SET_USER_MEMORY_REGION, &memregion))
    err (1, "KVM_SET_USER_MEMORY_REGION failed");

  /* Previously the following second slot was not needed because the
     virtual machine started at address 0xffff0 like the legacy 8086
     processor.  But now the virtual machine looks like starting at
     0xfffffff0 i.e. cs base 0xffff0000 ip 0xfff0 like a physical
     machine.  The following second slot for a page at 0xfffff000
     makes physical address 0xfffffff0 accessible. */
  struct kvm_userspace_memory_region memregion2;
  memregion2.slot = 1;
  memregion2.flags = 0;
  memregion2.guest_phys_addr = 0xfffff000;
  memregion2.userspace_addr = memregion.userspace_addr + 0xff000;
  memregion2.memory_size = 0x1000;
  if (ioctl (vmfd, KVM_SET_USER_MEMORY_REGION, &memregion2))
    err (1, "KVM_SET_USER_MEMORY_REGION failed (for 0xfffff000)");

  /* Create a vcpu */
  int vcpufd = ioctl (vmfd, KVM_CREATE_VCPU, 0);
  if (vcpufd < 0)
    err (1, "KVM_CREATE_VCPU failed");

  if (dump_initial_regs)
    dump_vcpu_regs (vcpufd);

  /* Set cpuid entry */
  struct {
    struct kvm_cpuid a;
    struct kvm_cpuid_entry b[4];
  } cpuid_info;
  cpuid_info.a.nent = 4;
  cpuid_info.a.entries[0].function = 0;
  cpuid_info.a.entries[0].eax = 1;
  cpuid_info.a.entries[0].ebx = 0;
  cpuid_info.a.entries[0].ecx = 0;
  cpuid_info.a.entries[0].edx = 0;
  cpuid_info.a.entries[1].function = 1;
  cpuid_info.a.entries[1].eax = 0x400;
  cpuid_info.a.entries[1].ebx = 0;
  cpuid_info.a.entries[1].ecx = 0;
  cpuid_info.a.entries[1].edx = 0x701b179; /* SSE2, SSE, FXSR, PAT,
					      CMOV, PGE, MTRR, CX8,
					      PAE, MSR, TSC, PSE,
					      FPU */
  cpuid_info.a.entries[2].function = 0x80000000;
  cpuid_info.a.entries[2].eax = 0x80000001;
  cpuid_info.a.entries[2].ebx = 0;
  cpuid_info.a.entries[2].ecx = 0;
  cpuid_info.a.entries[2].edx = 0;
  cpuid_info.a.entries[3].function = 0x80000001;
  cpuid_info.a.entries[3].eax = 0;
  cpuid_info.a.entries[3].ebx = 0;
  cpuid_info.a.entries[3].ecx = 0;
  cpuid_info.a.entries[3].edx = 0x20100800; /* AMD64, NX, SYSCALL */
  if (ioctl (vcpufd, KVM_SET_CPUID, &cpuid_info.a) < 0)
    err (1, "KVM_SET_CPUID failed");

  /* Mmap vcpu */
  int vcpu_mmap_size = ioctl (fd, KVM_GET_VCPU_MMAP_SIZE, 0);
  if (vcpu_mmap_size < 0)
    err (1, "KVM_GET_VCPU_MMAP_SIZE failed");
  struct kvm_run *kvm_run_structure = mmap (NULL, vcpu_mmap_size,
					    PROT_READ | PROT_WRITE, MAP_SHARED,
					    vcpufd, 0);
  if (kvm_run_structure == MAP_FAILED)
    err (1, "mmap vcpu");

  if (simple_run_test)
    {
      mem[0xffff0] = 0xf4;		/* hlt */
      mem[0xffff1] = 0xeb;		/* jmp */
      mem[0xffff2] = 0xfd;
      if (ioctl (vcpufd, KVM_RUN, 0))
	err (1, "KVM_RUN failed");
      /* The exit reason should be KVM_EXIT_HLT == 5. */
      warnx ("Exit_reason: %" PRIx32, kvm_run_structure->exit_reason);
      dump_vcpu_regs (vcpufd);
      exit (0);
    }

  /* Load kernel */
  uint32_t startaddr = load_linux (kernel, mem, memregion.memory_size, initrd);

  /* Put start up code */
  mem[0xffff0] = 0xea;		/* ljmp 0xf000, 0 */
  mem[0xffff1] = 0;
  mem[0xffff2] = 0;
  mem[0xffff3] = 0;
  mem[0xffff4] = 0xf0;

  extern char codestart[], codeend[];
  memcpy (mem + 0xf0000, codestart, codeend - codestart);
  memcpy (mem + 0xffffc, &startaddr, sizeof startaddr);

  /* Setup timer */
  signal (SIGALRM, sigalrm);

  /* Setup terminal */
  atexit (unstty);
  system ("stty raw -echo");

  /* Main loop */
  for (;;)
    {
      static uint8_t irqbase = 0x8, irqmask = 0xff;
      static int irqnotend;
      static uint8_t serial_ier, serial_flag, serial_input;
      static uint16_t timer0, timer2;
      static int halt;
      if (!serial_input)
	{
	  fd_set read_fds;
	  struct timeval tv;
	  tv.tv_sec = 0;
	  tv.tv_usec = 0;
	  FD_ZERO (&read_fds);
	  FD_SET (0, &read_fds);
	  if (select (1, &read_fds, NULL, NULL, &tv) == 1)
	    serial_input = 1;
	}
      if (kvm_run_structure->if_flag && !irqnotend)
	{
	  if (timer_interrupt_count && !(irqmask & 1))
	    {
	      timer_interrupt_count--;
	      struct kvm_interrupt interrupt;
	      interrupt.irq = irqbase;
	      if (ioctl (vcpufd, KVM_INTERRUPT, &interrupt))
		err (1, "KVM_INTERRUPT failed");
	      irqnotend = 1;
	    }
	  else if (((serial_flag && (serial_ier & 2)) ||
		    (serial_input && (serial_ier & 1))) && !(irqmask & 16))
	    {
	      struct kvm_interrupt interrupt;
	      interrupt.irq = irqbase + 4;
	      if (ioctl (vcpufd, KVM_INTERRUPT, &interrupt))
		err (1, "KVM_INTERRUPT failed");
	      irqnotend = 1;
	    }
	}
      if (halt)
	{
	  if (!irqnotend)
	    {
	      pause ();
	      continue;
	    }
	  halt = 0;
	}
      if (ioctl (vcpufd, KVM_RUN, 0))
	{
	  if (errno == EINTR)	/* For debugging and timer */
	    continue;
	  err (1, "KVM_RUN failed");
	}
      switch (kvm_run_structure->exit_reason)
	{
	  uint8_t *io_data, io_size;
	case KVM_EXIT_IO:
	  io_data = kvm_run_structure->io.data_offset +
	    (uint8_t *)kvm_run_structure;
	  io_size = kvm_run_structure->io.size;
	  switch (kvm_run_structure->io.direction)
	    {
	      static int timer0flag, timer2flag;
	      static uint8_t serial_lcr, serial_mcr;
	      static uint8_t value0x70;
	      static struct timeval timer2tv;
	    case KVM_EXIT_IO_OUT:
	      switch (kvm_run_structure->io.port)
		{
		  /* Primary PIC */
		  {
		    static int initstate;
		    uint8_t data;
		  case 0x20:
		    memcpy (&data, io_data, sizeof data);
		    if (data == 0x11)
		      initstate = 3;
		    if (data & 0x20)
		      irqnotend = 0;
		    continue;
		  case 0x21:
		    memcpy (&data, io_data, sizeof data);
		    if (initstate && initstate-- == 3)
		      irqbase = data;
		    else
		      irqmask = data;
		    continue;
		  }
		case 0x40:	/* PIT */
		  if (timer0flag ^= 1)
		    timer0 = *io_data;
		  else
		    {
		      timer0 |= (uint16_t)*io_data << 8;
		      uint64_t usec = timer0;
		      usec *= 1000000;
		      usec /= 1193182;
		      struct itimerval itv;
		      itv.it_interval.tv_sec = 0;
		      itv.it_interval.tv_usec = usec;
		      itv.it_value.tv_sec = 0;
		      itv.it_value.tv_usec = usec;
		      setitimer (ITIMER_REAL, &itv, NULL);
		    }
		  continue;
		case 0x42:	/* PIT */
		  if (timer2flag ^= 1)
		    timer2 = *io_data;
		  else
		    {
		      timer2 |= (uint16_t)*io_data << 8;
		      uint64_t usec = timer2;
		      usec *= 1000000;
		      usec /= 1193182;
		      if (gettimeofday (&timer2tv, NULL) < 0)
			err (1, "gettimeofday");
		      timer2tv.tv_sec += usec / 1000000;
		      timer2tv.tv_usec += usec % 1000000;
		      while (timer2tv.tv_usec >= 1000000)
			timer2tv.tv_usec -= 1000000, timer2tv.tv_sec++;
		    }
		  continue;
		case 0x43:	/* PIT */
		  timer0flag = timer2flag = 0;
		  continue;
		  /* COM1 */
		  {
		  case 0x3f8:
		    if (!(serial_lcr & 0x80))
		      if (write (1, io_data, 1) != 1)
			err (1, "Write to stdout");
		    console_initialized = 1;
		    serial_flag = 1;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3f9:
		    if (!(serial_lcr & 0x80))
		      serial_ier = *io_data;
		    serial_flag = 1;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3fa:
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3fb:
		    serial_lcr = *io_data;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3fc:
		    serial_mcr = *io_data;
		    continue;
		  case 0x3fd:
		  case 0x3fe:
		  case 0x3ff:
		    continue;
		  }
		case 0x70:	/* CMOS */
		  value0x70 = *io_data;
		  continue;
		case 0x60:	/* PS/2 */
		case 0x61:	/* Port 0x61 */
		case 0x64:	/* PS/2 */
		case 0x71:	/* CMOS */
		case 0xa0:	/* Secondary PIC */
		case 0xa1:	/* Secondary PIC */
		case 0xed:	/* Unknown */
		case 0x279:	/* Unknown */
		case 0x2e9:	/* COM4 */
		case 0x2f9:	/* COM2 */
		case 0x3d4:	/* Video */
		case 0x3d5:	/* Video */
		case 0x3e9:	/* COM3 */
		case 0x4d0:	/* Unknown */
		case 0x4d1:	/* Unknown */
		case 0xa79:	/* Unknown */
		case 0xc80:	/* EISA */
		case 0xcf8:	/* PCI configuration */
		case 0xcfa:	/* PCI configuration */
		case 0xcfb:	/* PCI configuration */
		case 0x1c80:	/* EISA */
		case 0x2c80:	/* EISA */
		case 0x3c80:	/* EISA */
		case 0x4c80:	/* EISA */
		case 0x5c80:	/* EISA */
		case 0x6c80:	/* EISA */
		case 0x7c80:	/* EISA */
		case 0x8c80:	/* EISA */
		  continue;
		}
	      warnx ("out %" PRIx16 " %" PRIx8 " %" PRIx8 " %" PRIx32 "\r",
		     kvm_run_structure->io.port, *io_data,
		     kvm_run_structure->io.size, kvm_run_structure->io.count);
	      continue;
	    case KVM_EXIT_IO_IN:
	      switch (kvm_run_structure->io.port)
		{
		case 0x21:	/* Primary PIC */
		  memset (io_data, irqmask, io_size);
		  continue;
		case 0x40:	/* PIT */
		  memset (io_data, 0, io_size);
		  {
		    static union {
		      uint8_t b[2];
		      uint16_t w;
		    } pit_hack;
		    if (timer0flag ^= 1)
		      {
			pit_hack.w = (pit_hack.w + 1) % timer0;
			*io_data = pit_hack.b[0];
		      }
		    else
		      *io_data = pit_hack.b[1];
		  }
		  continue;
		case 0x42:	/* PIT */
		  memset (io_data, 0, io_size);
		  {
		    static union {
		      uint8_t b[2];
		      uint16_t w;
		    } pit_hack;
		    if (timer2flag ^= 1)
		      {
			pit_hack.w = (pit_hack.w + 1) % timer2;
			*io_data = pit_hack.b[0];
		      }
		    else
		      *io_data = pit_hack.b[1];
		  }
		  continue;
		case 0x61:	/* Port 0x61 */
		  {
		    struct timeval tv;
		    if (gettimeofday (&tv, NULL) < 0)
		      err (1, "gettimeofday");
		    memset (io_data, 0, io_size);
		    if (tv.tv_sec > timer2tv.tv_sec ||
			(tv.tv_sec == timer2tv.tv_sec &&
			 tv.tv_usec > timer2tv.tv_usec))
		      *io_data = 0x20;
		  }
		  continue;
		case 0x71:	/* CMOS */
		  {
		    static uint8_t cmos_hack;
		    memset (io_data, 0, io_size);
		    time_t now = time (NULL);
		    struct tm *t = gmtime (&now);
		    switch (value0x70)
		      {
		      case 0x0:
			*io_data = bcd (t->tm_sec);
			break;
		      case 0x2:
			*io_data = bcd (t->tm_min);
			break;
		      case 0x4:
			*io_data = bcd (t->tm_hour);
			break;
		      case 0x6:
			*io_data = bcd (t->tm_wday + 1);
			break;
		      case 0x7:
			*io_data = bcd (t->tm_mday);
			break;
		      case 0x8:
			*io_data = bcd (t->tm_mon + 1);
			break;
		      case 0x9:
			*io_data = bcd (t->tm_year);
			break;
		      case 0xa:
			*io_data = cmos_hack ^= 0x80;
			break;
		      case 0xd:
			*io_data = 0x80;
			break;
		      }
		  }
		  continue;
		  /* COM1 */
		  {
		  case 0x3f8:
		    *io_data = 0;
		    if (!(serial_lcr & 0x80))
		      {
			if (serial_input && read (0, io_data, 1) != 1)
			  err (1, "Read from stdin");
			serial_input = 0;
		      }
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3f9:
		    *io_data = serial_ier;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3fa:
		    if ((serial_flag && (serial_ier & 2)) ||
			(serial_input && (serial_ier & 1)))
		      {
			*io_data = 0;
			serial_flag = 0;
		      }
		    else
		      *io_data = 1;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3fb:
		    *io_data = 3;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3fc:
		    *io_data = serial_mcr;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3fd:
		    *io_data = 0x60 | serial_input;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3fe:
		    *io_data = 0x80;
		    if (io_size-- == 1)
		      continue;
		    io_data++;
		  case 0x3ff:
		    memset (io_data, 0, io_size);
		    continue;
		  }
		case 0x404:	/* ACPI PM1a */
		  memset (io_data, 0, io_size);
		  continue;
		case 0x408:	/* ACPI Power Management Timer */
		  {
		    struct timeval tv;
		    if (gettimeofday (&tv, NULL) < 0)
		      err (1, "gettimeofday");
		    uint64_t tmp = tv.tv_usec;
		    tmp *= 3579545;
		    tmp /= 1000000;
		    tmp += tv.tv_sec * 3579545;
		    tmp &= 0xffffff;
		    memcpy (io_data, &tmp, io_size);
		  }
		  continue;
		case 0x60:	/* PS/2 */
		case 0x64:	/* PS/2 */
		case 0x213:	/* Unknown */
		case 0x233:	/* Unknown */
		case 0x253:	/* Unknown */
		case 0x273:	/* Unknown */
		case 0x2e9:	/* COM4 */
		case 0x2f9:	/* COM2 */
		case 0x393:	/* Unknown */
		case 0x3b3:	/* Unknown */
		case 0x3d3:	/* Unknown */
		case 0x3e9:	/* COM3 */
		case 0x3f3:	/* Unknown */
		case 0x4d0:	/* Unknown */
		case 0x4d1:	/* Unknown */
		case 0xc80:	/* EISA */
		case 0xcf8:	/* PCI configuration */
		case 0xcfc:	/* PCI configuration */
		case 0xcfe:	/* PCI configuration */
		case 0x1c80:	/* EISA */
		case 0x2c80:	/* EISA */
		case 0x3c80:	/* EISA */
		case 0x4c80:	/* EISA */
		case 0x5c80:	/* EISA */
		case 0x6c80:	/* EISA */
		case 0x7c80:	/* EISA */
		case 0x8c80:	/* EISA */
		  memset (io_data, 0xff, io_size);
		  continue;
		}
	      warnx ("in  %" PRIx16 " %" PRIx8 " %" PRIx32 "\r",
		     kvm_run_structure->io.port,
		     kvm_run_structure->io.size, kvm_run_structure->io.count);
	      continue;
	    }
	case KVM_EXIT_HLT:
	  {
	    struct kvm_regs regs;
	    if (ioctl (vcpufd, KVM_GET_REGS, &regs))
	      err (1, "KVM_GET_REGS failed");
	    if (!(regs.rflags & 0x200))
	      {
		dump_vram (mem);
		errx (1, "cli;hlt\r");
	      }
	  }
	  halt = 1;
	  continue;
	default:
	  dump_vram (mem);
	  dump_vcpu_regs (vcpufd);
	  errx (1, "Exit_reason: %" PRIx32, kvm_run_structure->exit_reason);
	}
    }
  exit (0);
}
